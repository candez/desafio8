import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { ContactoComponent } from './contacto/contacto.component';
import { FormularioComponent } from './formulario/formulario.component';
import { TestComponent } from './test/test.component';



@NgModule({
  declarations: [
    InicioComponent,
    ServiciosComponent,
    GaleriaComponent,
    ContactoComponent,
    FormularioComponent,
    TestComponent
  ],
  imports: [
    CommonModule
  ]
})
export class RoutesModule { }
